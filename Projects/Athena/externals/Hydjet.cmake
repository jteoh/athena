#
# File specifying the location of Hydjet to use.
#

set( HYDJET_LCGVERSION 1.6 )
set( HYDJET_LCGROOT
   ${LCG_RELEASE_DIR}/MCGenerators/hydjet/${HYDJET_LCGVERSION}/${LCG_PLATFORM} )
